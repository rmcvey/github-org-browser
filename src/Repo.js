import React, { Component } from 'react'
import moment from 'moment'
import classNames from 'classnames'
import Commit from './Commit'
import format from './utils/number-format'
import './Repo.css'

export default class Repo extends Component {
  render () {
    const { repo, commits, selectRepo, isCurrentRepo } = this.props

    return (
      <div className='repo'>
        <article className='media'>
          <figure className='media-left'>
            <p className='image is-64x64'>
              <img src={repo.owner.avatar_url} alt='Organization Logo' />
            </p>
          </figure>
          <div className='media-content'>
            <div className='content'>
              <p>
                <strong>{repo.name}</strong> <small>updated {moment(repo.updated_at).fromNow()}</small>
                <br />
                {repo.description}
              </p>
            </div>
            <nav className='level is-mobile'>
              <div className='level-left'>
                <a
                  className='button is-info is-outlined'
                  onClick={() => selectRepo(this.props.repo)}
                >
                  {commits.length ? 'Collapse' : 'View'} Recent Commits
                </a>
              </div>
              <div className='level-right'>
                <div className='level-item'>
                  <div className='tags has-addons'>
                    <span className='tag'>{format(repo.forks_count)}</span>
                    <span className='tag is-primary'>
                      <span className='icon is-small'><i className='fa fa-code-fork' /></span>
                    </span>
                  </div>
                </div>
                <div className='level-item'>
                  <div className='tags has-addons'>
                    <span className='tag'>{format(repo.stargazers_count)}</span>
                    <span className='tag is-primary'>
                      <span className='icon is-small'><i className='fa fa-star' /></span>
                    </span>
                  </div>
                </div>
              </div>
            </nav>
            <div className={classNames('commits', { hide: !isCurrentRepo })}>
              {commits.map(commit => <Commit key={commit.sha} commit={commit} />)}
              <hr />
            </div>
          </div>
        </article>
      </div>
    )
  }
}
