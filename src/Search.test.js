import React from 'react'
import ReactDOM from 'react-dom'
import Search from './Search'
import { expect } from 'chai'
import { mount, shallow } from 'enzyme'
import sinon from 'sinon'

const noop = () => {}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Search setOrg={noop} org='netflix' onSubmit={noop} />, div)
})

it('responds to click events', () => {
  const onSubmit = sinon.spy()
  const wrapper = shallow((
    <Search setOrg={noop} org='netflix' onSubmit={onSubmit} />
  ))
  wrapper.find('a').simulate('click')
  expect(onSubmit).to.have.property('callCount', 1)
})
