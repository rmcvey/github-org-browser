import React from 'react'
import ReactDOM from 'react-dom'
import Pagination from './Pagination'
import sinon from 'sinon'

const mockPagination = {
  pageLeftLink: 'https://news.ycombinator.com/',
  pageLeftText: 'prev',
  pageRightLink: 'https://news.ycombinator.com/news?p=2',
  pageRightText: 'next'
}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Pagination pagination={mockPagination} onClick={sinon.mock()} />, div)
})

it('should not render if no pagination is provided', () => {
  // TODO
})
