import React from 'react'
import ReactDOM from 'react-dom'
import Error from './Error'

const mockError = new Error('Just testing things out')

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Error error={mockError} />, div)
})
