import React, { Component } from 'react'
import Repo from './Repo'
import Search from './Search'
import Pagination from './Pagination'
import AppError from './Error'
import Github from './Github'
import '../node_modules/bulma/css/bulma.css'
import './App.css'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      org: props.defaultOrg,
      repos: [],
      commits: [],
      repo: false,
      sortKey: 'forks_count',
      pagination: null
    }

    const cachingEnabled = process.env.NODE_ENV === 'development'
    this.github = new Github(cachingEnabled)

    this.sortRepos = this.sortRepos.bind(this)
    this.selectRepo = this.selectRepo.bind(this)
    this.setOrg = this.setOrg.bind(this)
  }

  componentDidMount () {
    this.loadRepos()
  }

  // loading from an org name or pagination URL is the same process,
  // combining the two paths here
  async loadRepos (org = this.state.org, fromPage = false) {
    let promise

    if (fromPage) {
      promise = this.github.loadPage(org)
    } else {
      promise = this.github.loadOrganizationRepos(org)
    }

    try {
      const { data: repos, pagination } = await promise
      repos.sort(this.sortRepos)
      this.setState({
        repos,
        pagination,
        error: false,
        repo: false
      }, () => {
        window.scrollTo(0, 0)
      })
    } catch ({ message }) {
      this.setState({ error: message, repos: [], repo: false })
    }
  }

  async selectRepo (repo) {
    const { repo: currentRepo } = this.state

    // collapse if already selected
    if (currentRepo && repo.full_name === currentRepo.full_name) {
      return this.setState({ repo: false, commits: [] })
    }

    try {
      const { data: commits } = await this.github.loadCommits(repo)
      this.setState({ commits, repo })
    } catch ({ message }) {
      this.setState({ error: message, repo: false })
    }
  }

  // @TODO allow users to select which sortKey to use
  sortRepos (a, b) {
    return b[this.state.sortKey] - a[this.state.sortKey]
  }

  setOrg (org) {
    this.setState({ org })
  }

  render () {
    const { commits, repos, org, pagination, error } = this.state

    return (
      <section className='section'>
        <div className='container'>
          <h1 className='title'>Github Organization Browser</h1>

          <Search
            setOrg={this.setOrg}
            org={org}
            onSubmit={this.loadRepos.bind(this)} />

          {error ? <AppError message={error} /> : null}

          {repos.map(repo => {
            const isCurrentRepo = repo.full_name === this.state.repo.full_name
            return (
              <Repo
                repo={repo}
                key={repo.full_name}
                isCurrentRepo={isCurrentRepo}
                selectRepo={this.selectRepo}
                commits={isCurrentRepo ? commits : []} />
            )
          })}

          <Pagination
            pagination={pagination}
            onClick={this.loadRepos.bind(this)} />

        </div>
      </section>
    )
  }
}

export default App
