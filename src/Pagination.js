import React from 'react'

export default function Pagination (props) {
  const { pagination, onClick } = props

  if (!pagination) return null
  if (Object.keys(pagination).length === 0) return null

  return (
    <nav className='pagination is-centered'>
      <ul className="pagination-list">
        <li>
          <a className='pagination-link'
        onClick={() => onClick(pagination.pageLeftLink, true)}>{pagination.pageLeftText}</a>
        </li>
        <li>
          <a className='pagination-next'
            onClick={() => onClick(pagination.pageRightLink, true)}>{pagination.pageRightText}</a>
        </li>
      </ul>
    </nav>
  )
}
