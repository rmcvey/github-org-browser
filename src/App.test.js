import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import Repo from './Repo'
import { expect } from 'chai'
import { shallow } from 'enzyme'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App />, div)
})

it('renders multiple <Repo /> components', () => {
  const wrapper = shallow(<App defaultOrg='netflix' />)
  setTimeout(() => {
    expect(wrapper.find(Repo)).to.have.lengthOf.at.least(1)
  }, 1500)
})

it('renders an error if invalid org is specified', () => {
  const wrapper = shallow(<App defaultOrg='testTestTEST1234' />)
  setTimeout(() => {
    expect(wrapper.find('.is-warning')).to.have.length(1)
  }, 1500)
})
