import fetch from 'node-fetch'
import Cache from './utils/cache'
import capitalize from './utils/capitalize'

// parses pagination links out of headers, gets JSON data
async function parse (response) {
  const links = response.headers.get('link')
  const linkPattern = /<(.*)>; rel="(\w+)"/
  const data = await response.json()

  if (!links) {
    return { pagination: {}, data }
  }

  // parse out links and text from the headers
  const [prev, next] = links.split(', ')
                            .map(link => link.split(linkPattern))
                            .map(([_, link, text]) => ({ link, text }))

  return {
    pagination: {
      pageLeftLink: prev.link,
      pageLeftText: capitalize(prev.text),
      pageRightLink: next.link,
      pageRightText: capitalize(next.text)
    },
    data
  }
}

export default class Github {
  constructor (cachingEnabled = false) {
    this.cachingEnabled = cachingEnabled
    this.baseURI = 'https://api.github.com'

    if (this.cachingEnabled) {
      this.cache = new Cache()
    }
  }

  loadOrganizationRepos (org) {
    const uri = `${this.baseURI}/orgs/${org}/repos`
    return this.request(uri, org)
  }

  loadCommits (repo) {
    const uri = `${this.baseURI}/repos/${repo.full_name}/commits`
    return this.request(uri, repo.full_name)
  }

  loadPage (uri) {
    return this.request(uri, null)
  }

  // general request method, parses out pagination headers and data
  request (uri, cacheKey) {
    // check the cache and return found data
    if (this.cachingEnabled && this.cache.get(cacheKey)) {
      const response = this.cache.get(cacheKey)
      return Promise.resolve(response)
    }

    return fetch(uri)
      .then(response => parse(response))
      .then(response => {
        if (!Array.isArray(response.data)) {
          throw new Error(response.data.message)
        }
        return response
      })
      .then(response => {
        if (this.cachingEnabled && cacheKey) this.cache.set(cacheKey, response)
        return response
      })
  }
}
