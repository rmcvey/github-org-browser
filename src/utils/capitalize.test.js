import capitalize from './capitalize'
import { expect } from 'chai'

it ('should capitalize a word', () => {
  expect(capitalize('hello')).to.equal('Hello')
})

it ('should throw an error if not a string', () => {
  expect(() => capitalize(42)).to.throw()
})
