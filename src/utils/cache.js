/**
 * localStorage wrapper that performs automatic serialization
 * usage:
 *  const cache = new Cache()
 *  cache.set('foo', { bar 'baz' })
 *  const fromStore = cache.get('foo')
 *  console.log(fromStore.bar)
 */
export default class Cache {
  set (key, data) {
    if (data) {
      let serialized = data
      if (typeof serialized !== 'string') {
        serialized = JSON.stringify(serialized)
      }
      localStorage.setItem(key, serialized)
    }
    return data
  }

  get (key) {
    try {
      const data = localStorage.getItem(key)
      return JSON.parse(data)
    } catch (e) {
      console.error('Cache error', e)
    }
    return null
  }
}
