export default function capitalize (word) {
  if (typeof word !== 'string') throw new Error('capitalize accepts strings only')

  return (word[0].toUpperCase() + word.slice(1))
}
