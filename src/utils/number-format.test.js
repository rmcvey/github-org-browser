import { expect } from 'chai'
import format from './number-format'

it ('should format integers properly', () => {
  expect(format(1234)).to.equal('1,234')
})

it ('should format floats properly', () => {
  expect(format(1234.56)).to.equal('1,234.56')
})
