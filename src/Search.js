import React, { Component } from 'react'
import classNames from 'classnames'

export default class Search extends Component {
  constructor(props) {
    super(props)
    this.state = { error: false }
    this.onSubmit = this.onSubmit.bind(this)
    this.onKeyUp = this.onKeyUp.bind(this)
  }

  onKeyUp (event) {
    if (event.which === 13) {
      this.onSubmit()
    } else {
      // clear the error when user types
      if (this.state.error) {
        this.setState({ error: false })
      }
    }
  }

  onSubmit(event) {
    if (!this.props.org) {
      this.setState({ error: true })
    } else {
      this.props.onSubmit()
    }
  }

  render () {
    const { props, state } = this
    const { error } = state

    return (
      <div className='field has-addons' onKeyUp={this.onKeyUp}>
        <div className='control is-expanded'>
          <input
            className={classNames('input', { 'is-danger': error })}
            spellCheck={false}
            type='text'
            onChange={e => props.setOrg(e.target.value)}
            defaultValue={props.org}
            placeholder='Enter Organization Name' />
          {error ? (
            <p className="help is-danger">
              Organization name is required
            </p>
          ) : null}
        </div>
        <div className='control'>
          <a onClick={this.onSubmit} className='button is-info'>
            Search
          </a>
        </div>
      </div>
    )
  }
}
