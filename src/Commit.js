import React from 'react'
import moment from 'moment'
import './Commit.css'

export default function Commit ({commit}) {
  let { committer } = commit
  // some older commits seem to randomly have null commit.committer
  // use commit.commit.committer in those cases
  if (!committer) committer = commit.commit.committer

  return (
    <article className='media commit'>
      <figure className='media-left'>
        <p className='image is-48x48'>
          <img src={committer.avatar_url || 'https://avatars3.githubusercontent.com/u/19864447?v=4'} alt='User Avatar' />
        </p>
      </figure>
      <div className='media-content'>
        <div className='content'>
          <p>
            <strong>{commit.commit.committer.name}</strong> - <small>{moment(commit.commit.committer.date).fromNow()}</small>
            <br />
            {commit.commit.message}
          </p>
        </div>
      </div>
    </article>
  )
}
