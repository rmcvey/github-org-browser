import React from 'react'

export default function Error ({ message }) {
  return (
    <div className='notification is-warning'>{message}</div>
  )
}
